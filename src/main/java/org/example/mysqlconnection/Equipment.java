package org.example.mysqlconnection;

import java.util.ArrayList;

public class Equipment {

    private int equipId;
    private String equipmentName;
    private String equipmentDescription;
    private int equipmentCapacity;
    private ArrayList<String> equipmentHeaders;

    ///////////////
    //CONSTRUCTORS
    //////////////
    /**
     * Default constructor
     */
    public Equipment() {
        this( 0, "", "", 0);
    }

    /**
     * parametrized constructor which accepts only equipId
     * @param equipId
     */
    public Equipment(int equipId) {
        this (equipId, "", "", 0);
    }

    /**
     * Parametrized constructor which accepts all attributes
     * @param equipId
     * @param equipmentName
     * @param equipmentDescription
     * @param equipmentCapacity
     */
    public Equipment(int equipId, String equipmentName, String equipmentDescription, int equipmentCapacity) {
        this.equipId = equipId;
        this.equipmentName = equipmentName;
        this.equipmentDescription = equipmentDescription;
        this.equipmentCapacity = equipmentCapacity;
        this.equipmentHeaders = null;
    }

    /////////////////////
    //GETTERS AND SETTERS
    /////////////////////
    /**
     * equipId getter
     * @return
     */
    public int getEquipId() { return equipId; }

    /**
     * equipId setter
     * @param equipId
     */
    public void setEquipId(int equipId) {
        this.equipId = equipId;
    }

    /**
     * equipmentName getter
     * @return
     */
    public String getEquipmentName() { return equipmentName; }

    /**
     * equipmentName setter
     * @param equipmentName
     */
    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    /**
     * equipmentDescription getter
     * @return
     */
    public String getEquipmentDescription() { return equipmentDescription; }

    /**
     * equipmentDescription setter
     * @param equipmentDescription
     */
    public void setEquipmentDescription(String equipmentDescription) {
        this.equipmentDescription = equipmentDescription;
    }

    /**
     * equipmentCapacity getter
     * @return
     */
    public int getEquipmentCapacity() { return equipmentCapacity; }

    /**
     * equipmentCapacity setter
     * @param equipmentCapacity
     */
    public void setEquipmentCapacity(int equipmentCapacity) {
        this.equipmentCapacity = equipmentCapacity;
    }

    //////////
    //METHODS
    /////////


    /**
     * fetch method which gets data from the MySQL database based on the equipment id and sets
     * new attribute values of this equipment object to ones from the database
     * @param msd
     */
    public void fetch(MySQLDatabase msd) throws DLException {
        try{
            fetch(msd, false);
        } catch (DLException e) {
            throw e;
        }
    }

    /**
     * fetch method which gets data from the MySQL database based on the equipment id and sets
     * new attribute values of this equipment object to ones from the database, accepts boolean for column names
     * @param msd
     * @param needColNames
     */
    public void fetch(MySQLDatabase msd, boolean needColNames) throws DLException {
        String sqlStatement = String.format("SELECT * FROM EQUIPMENT WHERE equipId = %d;", this.equipId);
        try {
            ArrayList<ArrayList<String>> fetchData = msd.getData(sqlStatement, needColNames);
            if (0 != fetchData.size()) {
                int dataIndex = 0;
                if (needColNames) {
                    equipmentHeaders = fetchData.get(0);
                    dataIndex = 1;
                }
                this.equipId = Integer.parseInt(fetchData.get(dataIndex).get(0));
                this.equipmentName = fetchData.get(dataIndex).get(1);
                this.equipmentDescription = fetchData.get(dataIndex).get(2);
                this.equipmentCapacity = Integer.parseInt(fetchData.get(dataIndex).get(3));
            } else System.out.println("\n   Fetch yielded no results for equipId = " + this.equipId);
        }
        catch (DLException e) {
            throw e;
        }
    }



    /**
     * put method will update the database values, for this object’s equipmentId,
     * using this object’s attribute values and setData method
     * from the MySQLDatabase object
     * @param msd
     */
    public void put(MySQLDatabase msd) throws DLException {
        String sqlStatement = String.format(
                "UPDATE EQUIPMENT SET " +
                "EquipmentName = '%s', " +
                "EquipmentDescription = '%s', " +
                "EquipmentCapacity = %d "  +
                "WHERE EquipId = %d;",
                this.equipmentName,
                this.equipmentDescription,
                this.equipmentCapacity,
                this.equipId
        );
        try {
            msd.setData(sqlStatement);
        }
        catch (DLException e) {
            throw e;
        }
    }

    /**
     * post method will insert this object’s attribute values into the database
     * as a new record (with specific and unique equipmentID)
     * using setData method from the MySQLDatabase object
     * @param msd
     */
    public void post(MySQLDatabase msd) throws DLException {
        String sqlStatement = String.format(
                "INSERT INTO EQUIPMENT VALUES (%d,'%s','%s',%d);",
                this.equipId,
                this.equipmentName,
                this.equipmentDescription,
                this.equipmentCapacity
        );
        try {
            msd.setData(sqlStatement);
        }
        catch (DLException e) {
            throw e;
        }
    }

    /**
     * remove method will delete from the database any data row
     * corresponding to the object’s equipmentId using setData method
     * @param msd
     */
    public void remove(MySQLDatabase msd) throws DLException {
        String sqlStatement = String.format(
                "DELETE FROM EQUIPMENT WHERE equipId = %d;",
                this.equipId
        );
        try {
            msd.setData(sqlStatement);
        }
        catch (DLException e) {
            throw e;
        }
    }

    public void printEquipment(){
        if(equipmentHeaders == null || 0 == equipmentHeaders.size()) {
            System.out.println(String.format(
                    "\n%15s | %15s | %22s | %20s",
                    "Equipment ID", "Equipment Name", "Equipment Description", "Equipment Capacity"));
        }
        else {
            System.out.println(String.format(
                    "\n%15s | %15s | %22s | %20s",
                    equipmentHeaders.get(0), equipmentHeaders.get(1), equipmentHeaders.get(2), equipmentHeaders.get(3) ) );
        }
        System.out.println(String.format(
                "%15d   %15s   %22s   %20d\n",
                equipId,equipmentName,equipmentDescription,equipmentCapacity
        ));
    }

}
