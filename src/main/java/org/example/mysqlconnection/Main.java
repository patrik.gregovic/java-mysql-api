package org.example.mysqlconnection;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {
    // Note to self:
    // "jdbc:mysql://localhost:3306/travel?&serverTimezone=CET"
    // "root"
    // "root"
    // had to include timezone due to issues with mysql server
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.printf("Please enter your connection url: ");
        String connectionUrl = sc.nextLine();
        System.out.printf("Please enter your username: ");
        String connectionUsername = sc.nextLine();
        System.out.printf("Please enter your password: ");
        String connectionPassword = sc.nextLine();

        /* Hardcoded values I used for faster testing on my machine */
        /*
        String connectionUrl = "jdbc:mysql://localhost:3306/travel?&serverTimezone=CET";
        String connectionUsername = "root";
        String connectionPassword = "root";
        */
        MySQLDatabase msd = new MySQLDatabase(connectionUrl, connectionUsername, connectionPassword);

        try {
            msd.connect();
            System.out.println("Connection established.");
        } catch (DLException e) {
            System.out.println("Unable to establish connection.");
            System.out.println(e.getUserMessage());
            return;
        }
        try {
            System.out.println("\nTest Fetch:");
            Equipment equipment1 = new Equipment();
            equipment1.setEquipId(568);
            equipment1.fetch(msd);//test fetch
            equipment1.printEquipment();

            System.out.println("\nTest Put:");
            equipment1.setEquipmentCapacity(4000);
            equipment1.setEquipmentName("Country");
            equipment1.setEquipmentDescription("Only passenger");
            equipment1.put(msd); //test put
            equipment1.printEquipment();

            System.out.println("\nTest Post:");
            Equipment equipment2 = new Equipment(1337, "Eco plane", "Passenger only", 7331);
            equipment2.post(msd); //test post
            equipment2.printEquipment();

            System.out.println("\nTest Remove:");
            equipment2.remove(msd);//test remove
            Equipment equipment3 = new Equipment();
            equipment3.setEquipId(1337);
            equipment3.fetch(msd);
            equipment3.printEquipment();

            System.out.println("\nTest Fetch with metadata:");
            Equipment equipment4 = new Equipment();
            equipment4.setEquipId(568);
            equipment4.fetch(msd, true);//test fetch
            equipment4.printEquipment();

            msd.printDBInfo();
            msd.printTableInfo("Equipment");
            msd.printResultSetInfo("SELECT * FROM EQUIPMENT");
            msd.printResultSetInfo("SELECT COUNT(*) FROM EQUIPMENT");

        }
        catch (DLException e) {
            System.out.println(e.getUserMessage());
        }


        try {
            msd.close();
            System.out.println("Connection closed.");
        } catch (DLException e) {
            System.out.println("Unable to close connection.");
            System.out.println(e.getUserMessage());
            return;
        }

    }
}
