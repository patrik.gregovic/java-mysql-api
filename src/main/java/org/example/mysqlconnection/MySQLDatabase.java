package org.example.mysqlconnection;

import com.mysql.cj.xdevapi.Result;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

public class MySQLDatabase {

    private String mysql;
    private String username;
    private String password;
    private Connection conn;
    private DatabaseMetaData databaseMetaData;
    /**
     *  Parametrized constructor
     * @param mysql
     * @param username
     * @param password
     */
    public MySQLDatabase(String mysql, String username, String password) {
        this.mysql = mysql;
        this.username = username;
        this.password = password;
        this.databaseMetaData = null;
    }

    /**
     * mysql getter
     * @return
     */
    public String getMysql() {
        return mysql;
    }

    /**
     * mysql setter
     * @param mysql
     */
    public void setMysql(String mysql) {
        this.mysql = mysql;
    }

    /**
     * username setter
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     * username setter
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * password getter
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     * password setter
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * method for connecting to the database
     * @return
     */
    public void connect() throws DLException {
        boolean success = false;
        try {
            this.conn = DriverManager.getConnection(this.mysql, this.username, this.password);
            this.databaseMetaData = conn.getMetaData();
            success = true;
        }
        catch (SQLException e) {
            throw new DLException(e, "Failed to connect to database with given information", (new Date()).toString() );
        }
    }

    /**
     * method for closing the connection to the database
     * @return
     */
    public void close() throws DLException {
        try {
            if(this.conn != null)
                this.conn.close();
        }
        catch (SQLException e) {
            throw new DLException(e, "Failed to close the connection to the database.", (new Date()).toString() );
        }
    }

    /**
     * getData method, accepts sql statement and returns
     * a 2d ArrayList of retrieved data,
     * returns null if statement failed to execute
     * @param sqlStatement
     * @return
     */
    public ArrayList<ArrayList<String>> getData(String sqlStatement) throws DLException {
        try {
            return getData(sqlStatement, false);
        }
        catch (DLException e) {
            throw e;
        }
    }

    /**
     * getData method, accepts sql statement and a boolean
     * returns a two dimensional array of strings
     * @param sqlStatement
     * @return
     */
    public ArrayList<ArrayList<String>> getData(String sqlStatement, Boolean needColNames) throws DLException {
            if (sqlStatement.contains("SELECT")) {
                try {
                    Statement stmt = conn.createStatement();
                    ResultSet rs = stmt.executeQuery(sqlStatement);
                    int columnCount = rs.getMetaData().getColumnCount();
                    ArrayList<ArrayList<String>> retrievedData = new ArrayList<ArrayList<String>>();
                    if(needColNames) {
                        String tableNamePattern = rs.getMetaData().getTableName(1);
                        ResultSet resultMetadata = databaseMetaData.getColumns(null, null, tableNamePattern, null);
                        ArrayList<String> rowContents = new ArrayList<String>();
                        while(resultMetadata.next()){
                            rowContents.add(resultMetadata.getString(4));
                        }
                        retrievedData.add(rowContents);
                    }
                    int i = 0;
                    while (rs.next()) {
                        ArrayList<String> rowContents = new ArrayList<String>();
                        for (int j = 0; j < columnCount; j++) {
                            rowContents.add(rs.getString(j + 1));
                        }
                        retrievedData.add(rowContents);
                        i++;
                    }
                    return retrievedData;
                }
                catch (SQLException e) {
                    throw new DLException(e, "Failed to run desired query methods from java.sql in lines 113-115.", (new Date()).toString());
                }
            }
        return null;
    }

    /**
     * setData method, accepts an sql statement, returns false if
     * statement couldnt be executed, returns true if statement was
     * executed successfully
     * @param sqlStatement
     * @return
     */
    public boolean setData(String sqlStatement) throws DLException {
        if(sqlStatement.contains("UPDATE") || sqlStatement.contains("DELETE") || sqlStatement.contains("INSERT")) {
            try {
                Statement stmt = conn.createStatement();
                return (0 < stmt.executeUpdate(sqlStatement));
            }
            catch (SQLException e) {
                String method = sqlStatement.contains("UPDATE") ? "UPDATE" :
                                sqlStatement.contains("DELETE") ? "DELETE" :
                                                                  "INSERT" ;
                throw new DLException(e, "Failed to modify data using the requested "+method+" statement.", (new Date()).toString() );
            }
        }
        return false;
    }

    /**
     * Method for printing out database information
     * prints product name/version, driver name/version, lists all tables with their types
     * and prints support for group by, outer join and statement pooling
     * @throws DLException
     */
    public void printDBInfo() throws DLException {
        try {
            System.out.println("Database product name: " + databaseMetaData.getDatabaseProductName());
            System.out.println("Database product version: " + databaseMetaData.getDatabaseProductVersion());
            System.out.println("Database driver name: " + databaseMetaData.getDriverName());
            System.out.println("Database driver version: " + databaseMetaData.getDriverVersion());
            System.out.println("List of table names and their types: ");
            ResultSet result =   databaseMetaData.getTables ( null, null, null, null );
            while(result.next()){
                System.out.println("Name: " + result.getString(3) + " | Type: " + result.getString(4));
            }
            System.out.println("Supports group by: " + databaseMetaData.supportsGroupBy());
            System.out.println("Supports outer join: " + databaseMetaData.supportsOuterJoins());
            System.out.println("Supports statement pooling: " + databaseMetaData.supportsStatementPooling()+"\n");
        }
        catch (SQLException e) {
            throw new DLException(e, "Failed to get database information from metadata.", (new Date()).toString());
        }
    }

    /**
     * Method printTableInfo accepts a table name of an existing table in the database
     * and prints to standard output available information about it, including column count,
     * column names,and column types, as well as primary keys
     * @param tableName
     * @throws DLException
     */
    public void printTableInfo(String tableName) throws DLException{
        try {
            System.out.println("List of column names and types: ");
            ResultSet result = databaseMetaData.getColumns( null, null, tableName, null);
            while(result.next()) {
                System.out.println("Name: " + result.getString(4) + " | Type: " + result.getInt(5));
            }
            result = databaseMetaData.getPrimaryKeys( null, null, tableName);
            System.out.println("Primary keys: ");
            while(result.next()) {
                System.out.println(result.getString(4));
            }
            System.out.print("\n");
        }
        catch (SQLException e) {
            throw new DLException(e, "Failed to get table information from metadata.", (new Date()).toString());
        }
    }

    /**
     *  Method printResultSetInfo accepts a query and prints to standard output
     *  available information about that query, including actual query, column count,
     *  column names, column types and possible use of column in WHERE clause.
     * @throws DLException
     */
    public void printResultSetInfo(String query) throws DLException{
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            System.out.println("Query: " + query);
            System.out.println("Column count: " + rsmd.getColumnCount());
            System.out.println("List of column names, types and searchability: ");
            for(int i=1; i<rsmd.getColumnCount()+1; i++)
                System.out.println("Name: " + rsmd.getColumnName(i) + " | Type: " + rsmd.getColumnTypeName(i) + " | Searchability: " + rsmd.isSearchable(i));
            System.out.print("\n");
        }
        catch (SQLException e) {
            throw new DLException(e, "Failed to get ResultSet information from metadata.", (new Date()).toString());
        }
    }



}
