package org.example.mysqlconnection;

import java.io.IOException;
import java.util.Date;
import java.sql.SQLException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class DLException extends SQLException {

    private String message;

    public DLException(SQLException sqle){
        this( sqle, "", (new Date()).toString() );
    }

    public DLException(SQLException sqle, String reason, String timeStamp){
        this.message =
                "<============[  "+sqle.getClass().getSimpleName()+"  happened at:  "+timeStamp+"  ]============>\n" +
                "Reason: "+reason+"\n" +
                "SQLState code: "+sqle.getSQLState()+"\n" +
                "Vendor error code: "+sqle.getErrorCode()+"\n" +
                "Stack trace:\n" + sqle.getStackTrace();
        log(this.message);
        this.message = "The program could not complete the desired operation. Please contact your administrator for help.";
    }

    public String getUserMessage() {
        return this.message;
    }

    private void log(String message){
        Logger logger = Logger.getLogger("sqxlExceptionLog");
        logger.setUseParentHandlers(false);
        FileHandler fh = null;
        try {
            fh = new FileHandler("sqlExceptionLog.txt");
            fh.setFormatter(new SimpleFormatter());
            logger.addHandler(fh);
            logger.log(Level.WARNING,message);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
